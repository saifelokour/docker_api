# DockerApi

Docker Engine API wrapper. Inspired [docker-api](https://github.com/swipely/docker-api)

## Installation

```elixir
def deps do
  [
    {:docker_api, "~> 0.1.1"}
  ]
end
```

## Usage

### Container

```elixir
# List containers.
iex> DockerApi.Container.list()
{:ok,
 [
   %DockerApi.Container{
     connection: %DockerApi.Connection{
       ipfamily: :local,
       unix_socket: '/var/run/docker.sock'
     },
     id: "1a50869e463662b4546c4dda0606e25165482080969ec690826db809d3ae9a70"
   }
 ]}

# Create a container.
iex> {:ok, container} = DockerApi.Container.create([query: [name: "test_container"], body: %{Image: "ubuntu:latest", Tty: true}])
{:ok,
 %DockerApi.Container{
   connection: %DockerApi.Connection{
     ipfamily: :local,
     unix_socket: '/var/run/docker.sock'
   },
   id: "cb5972cf722babb4fd2099da051d5850c59c92f314dec8262209d580ce6d4d8c"
 }}

# Start a container.
iex> DockerApi.Container.start(container)
{:ok,
 %DockerApi.Container{
   connection: %DockerApi.Connection{
     ipfamily: :local,
     unix_socket: '/var/run/docker.sock'
   },
   id: "cb5972cf722babb4fd2099da051d5850c59c92f314dec8262209d580ce6d4d8c"
 }}

# Stop a container.
iex> DockerApi.Container.stop(container)
{:ok,
 %DockerApi.Container{
   connection: %DockerApi.Connection{
     ipfamily: :local,
     unix_socket: '/var/run/docker.sock'
   },
   id: "cb5972cf722babb4fd2099da051d5850c59c92f314dec8262209d580ce6d4d8c"
 }}

# Restart a container.
iex> DockerApi.Container.restart(container)
{:ok,
 %DockerApi.Container{
   connection: %DockerApi.Connection{
     ipfamily: :local,
     unix_socket: '/var/run/docker.sock'
   },
   id: "cb5972cf722babb4fd2099da051d5850c59c92f314dec8262209d580ce6d4d8c"
 }}

# Inspect a container.
iex> DockerApi.Container.inspect(container)
{:ok,
 %{
   "AppArmorProfile" => "",
   "Args" => [],
   ...
 }}

# Pause a container.
iex> DockerApi.Container.pause(container)
{:ok,
 %DockerApi.Container{
   connection: %DockerApi.Connection{
     ipfamily: :local,
     unix_socket: '/var/run/docker.sock'
   },
   id: "cb5972cf722babb4fd2099da051d5850c59c92f314dec8262209d580ce6d4d8c"
 }}

# Unpause a container.
iex> DockerApi.Container.unpause(container)
{:ok,
 %DockerApi.Container{
   connection: %DockerApi.Connection{
     ipfamily: :local,
     unix_socket: '/var/run/docker.sock'
   },
   id: "cb5972cf722babb4fd2099da051d5850c59c92f314dec8262209d580ce6d4d8c"
 }}

# List processes running inside a container.
iex> DockerApi.Container.top(container)
{:ok,
 %{
   "Processes" => [
     ["root", "2046", "2019", "1", "22:56", "pts/0", "00:00:00", "/bin/bash"]
   ],
   "Titles" => ["UID", "PID", "PPID", "C", "STIME", "TTY", "TIME", "CMD"]
 }}

# Kill a container
iex> DockerApi.Container.kill(container)
{:ok,
 %DockerApi.Container{
   connection: %DockerApi.Connection{
     ipfamily: :local,
     unix_socket: '/var/run/docker.sock'
   },
   id: "cb5972cf722babb4fd2099da051d5850c59c92f314dec8262209d580ce6d4d8c"
 }}

# Remove a container
iex> DockerApi.Container.remove(container)
:ok
```